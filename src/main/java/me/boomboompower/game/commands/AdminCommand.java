/*
 *     Copyright (C) 2016 boomboompower
 *
 *     This program is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     This program is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU General Public License for more details.
 *
 *     You should have received a copy of the GNU General Public License
 *     along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package me.boomboompower.game.commands;

import me.boomboompower.game.Game;
import me.boomboompower.game.makers.CommandChecker;
import me.boomboompower.game.makers.ConfigGetter;
import me.boomboompower.game.types.GameState;

import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

import org.spigotmc.SpigotConfig;

import static me.boomboompower.game.api.MessageUtils.sendMessage;

public class AdminCommand implements CommandExecutor {

    private Game game;
    private String command = "admin";
    private String unknown = SpigotConfig.unknownCommandMessage;

    public AdminCommand(Game game) {
        this.game = game;

        game.getCommand(command).setExecutor(this);
    }

    @Override
    public boolean onCommand(CommandSender sender, Command cmd, String s, String[] args) {
        CommandChecker c = new CommandChecker(args);
        if (sender instanceof Player) {
            if (!sender.hasPermission("admin.command")) {
                sendMessage((Player) sender, unknown);
            } else {
                Player p = (Player) sender;
                ConfigGetter b = new ConfigGetter(p);
                if (!c.hasArgs()) {
                    sendMessage(p, "&5Hello there, this is a admin command.");
                    sendMessage(p, "&5There are quite a few options for this.");
                    sendMessage(p, "&5Ask boomboompower for some of them!");
                } else {
                    if (c.args(0, "health")) {
                        if (c.hasArgs(1)) {
                            b.setHealth(Integer.parseInt(c.args(1)));
                        } else {
                            sendMessage(p, unknown);
                        }
                    } else if (c.args(0, "start")) {
                        game.setGameState(GameState.STARTING);
                    } else if (c.args(0, "end") || c.args(0, "finish")) {
                        game.setGameState(GameState.FINISHED);
                    }
                }
            }
        }
        return true;
    }
}
