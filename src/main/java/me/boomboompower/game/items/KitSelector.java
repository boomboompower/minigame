/*
 *     Copyright (C) 2016 boomboompower
 *
 *     This program is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     This program is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU General Public License for more details.
 *
 *     You should have received a copy of the GNU General Public License
 *     along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package me.boomboompower.game.items;

import me.boomboompower.game.makers.ItemMaker;

import org.bukkit.Material;
import org.bukkit.inventory.ItemStack;

public class KitSelector {

    private KitSelector() {}

    public static ItemStack getSelector() {
        ItemMaker itemMaker = new ItemMaker(Material.NETHER_STAR);
        itemMaker.setLore("", "&7Right click to open kit selector!");
        itemMaker.setUnbreakable(true);
        itemMaker.setName("&bKit Selector &7(Right Click)", true);
        return itemMaker.getItemStack();
    }
}
