/*
 *     Copyright (C) 2016 boomboompower
 *
 *     This program is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     This program is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU General Public License for more details.
 *
 *     You should have received a copy of the GNU General Public License
 *     along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package me.boomboompower.game.items.kits;

import me.boomboompower.game.makers.ItemMaker;

import org.bukkit.Material;
import org.bukkit.enchantments.Enchantment;
import org.bukkit.entity.Player;
import org.bukkit.inventory.ItemFlag;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.PlayerInventory;

public class PYRO {

    private PYRO() {}

    public static ItemStack getIcon() {
        ItemMaker itemMaker = new ItemMaker(Material.FLINT_AND_STEEL);
        itemMaker.setName("&aPyro", true);
        itemMaker.setUnbreakable(true);
        itemMaker.setLore("&7Chainmail helmet &8(&7Fire protection II&8)", "&7Flint and Steel &8(&7Fire Aspect I&8, &7Sharpness II&8)", "&7Baked Potato &8x12");
        itemMaker.addFlag(ItemFlag.HIDE_ATTRIBUTES);
        return itemMaker.getItemStack();
    }

    public static ItemStack getHelmet() {
        ItemMaker itemMaker = new ItemMaker(Material.GOLD_HELMET);
        itemMaker.setUnbreakable(false);
        itemMaker.setDisplayName("&2Pyro Helmet");
        itemMaker.addEnchantment(Enchantment.PROTECTION_FIRE, 2);
        itemMaker.setLore("&9Their skin will crackle.");
        return itemMaker.getItemStack();
    }

    public static ItemStack getChestPlate() {
        return new ItemMaker(Material.AIR).getItemStack();
    }

    public static ItemStack getLeggings() {
        return new ItemMaker(Material.AIR).getItemStack();
    }

    public static ItemStack getBoots() {
        return new ItemMaker(Material.AIR).getItemStack();
    }

    public static ItemStack getWeapon() {
        ItemMaker itemMaker = new ItemMaker(Material.FLINT_AND_STEEL);
        itemMaker.setUnbreakable(false);
        itemMaker.setDisplayName("&2Pyro Weapon");
        itemMaker.addEnchantment(Enchantment.FIRE_ASPECT, 1);
        itemMaker.addEnchantment(Enchantment.DAMAGE_ALL, 2);
        itemMaker.setLore("&9Their skin will crackle.");
        itemMaker.addFlag(ItemFlag.HIDE_ENCHANTS);
        return itemMaker.getItemStack();
    }

    public static ItemStack getFood() {
        ItemMaker itemMaker = new ItemMaker(Material.BAKED_POTATO);
        itemMaker.setUnbreakable(true);
        itemMaker.setDisplayName("&2Pyro Food");
        itemMaker.setLore("&9Their skin will crackle.");
        itemMaker.setAmount(12);
        return itemMaker.getItemStack();
    }

    public static ItemStack getBow() {
        return new ItemMaker(Material.AIR).getItemStack();
    }

    public static ItemStack getArrow() {
        return new ItemMaker(Material.AIR).getItemStack();
    }

    public static void toPlayer(Player player) {
        PlayerInventory i = player.getInventory();
        i.setItem(0, getWeapon());
        i.setItem(1, getFood());
        i.setItem(8, getBow());
        i.setItem(27, getArrow());
        i.setHelmet(getHelmet());
        i.setChestplate(getChestPlate());
        i.setLeggings(getLeggings());
        i.setBoots(getBoots());
    }
}
