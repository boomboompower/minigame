/*
 *     Copyright (C) 2016 boomboompower
 *
 *     This program is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     This program is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU General Public License for more details.
 *
 *     You should have received a copy of the GNU General Public License
 *     along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package me.boomboompower.game.items.kits;

import me.boomboompower.game.makers.ItemMaker;

import org.bukkit.Material;
import org.bukkit.enchantments.Enchantment;
import org.bukkit.entity.Player;
import org.bukkit.inventory.ItemFlag;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.PlayerInventory;

public class DEFAULT {

    private DEFAULT() {}

    public static ItemStack getIcon() {
        ItemMaker itemMaker = new ItemMaker(Material.STONE_PICKAXE);
        itemMaker.setName("&aDefault", true);
        itemMaker.setUnbreakable(true);
        itemMaker.setLore("&7Stone pickaxe &8(&7Efficiency II&8, &7Unbreaking III&8)", "&7Stone axe &8(&7Efficiency II&8, &7Unbreaking III&8)", "&7Stone shovel &8(&7Efficiency II&8, &7Unbreaking III&8)");
        itemMaker.addFlag(ItemFlag.HIDE_ATTRIBUTES);
        return itemMaker.getItemStack();
    }

    public static ItemStack getPick() {
        ItemMaker itemMaker = new ItemMaker(Material.STONE_PICKAXE);
        itemMaker.setUnbreakable(false);
        itemMaker.setDisplayName("&2Pickaxe");
        itemMaker.addEnchantment(Enchantment.DIG_SPEED, 2);
        itemMaker.addEnchantment(Enchantment.DURABILITY, 3);
        return itemMaker.getItemStack();
    }

    public static ItemStack getAxe() {
        ItemMaker itemMaker = new ItemMaker(Material.STONE_AXE);
        itemMaker.setUnbreakable(false);
        itemMaker.setDisplayName("&2Axe");
        itemMaker.addEnchantment(Enchantment.DIG_SPEED, 2);
        itemMaker.addEnchantment(Enchantment.DURABILITY, 3);
        return itemMaker.getItemStack();
    }

    public static ItemStack getShovel() {
        ItemMaker itemMaker = new ItemMaker(Material.STONE_SPADE);
        itemMaker.setUnbreakable(false);
        itemMaker.setDisplayName("&2Shovel");
        itemMaker.addEnchantment(Enchantment.DIG_SPEED, 2);
        itemMaker.addEnchantment(Enchantment.DURABILITY, 3);
        return itemMaker.getItemStack();
    }

    public static void toPlayer(Player player) {
        PlayerInventory i = player.getInventory();
        i.setItem(0, getPick());
        i.setItem(1, getAxe());
        i.setItem(2, getShovel());
    }
}
