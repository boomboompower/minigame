/*
 *     Copyright (C) 2016 boomboompower
 *
 *     This program is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     This program is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU General Public License for more details.
 *
 *     You should have received a copy of the GNU General Public License
 *     along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package me.boomboompower.game.items.kits;

import me.boomboompower.game.makers.ItemMaker;

import org.bukkit.Material;
import org.bukkit.entity.Player;
import org.bukkit.inventory.ItemFlag;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.PlayerInventory;
import org.bukkit.potion.PotionEffectType;

public class WITCH {

    private WITCH() {}

    public static ItemStack getIcon() {
        ItemMaker itemMaker = new ItemMaker(Material.SPLASH_POTION);
        itemMaker.addPotionEffect(PotionEffectType.HEAL.createEffect(20, 5));
        itemMaker.setName("&aWitch", true);
        itemMaker.setUnbreakable(true);
        itemMaker.setLore("&7Splash potion &8(&7Instant Health II&8) x2", "&7Splash potion &8(&7Regeneration III&8) x3", "&7Splash Potion &8(&7Instant Damage I&8) x4");
        itemMaker.addFlag(ItemFlag.HIDE_POTION_EFFECTS, ItemFlag.HIDE_ATTRIBUTES);
        return itemMaker.getItemStack();
    }

    public static ItemStack getWeapon() {
        ItemMaker itemMaker = new ItemMaker(Material.SPLASH_POTION);
        itemMaker.addPotionEffect(PotionEffectType.HEAL.createEffect(20, 2));
        itemMaker.setDisplayName("&2Health Potion");
        itemMaker.addFlag(ItemFlag.HIDE_POTION_EFFECTS);
        itemMaker.setLore("&aInstant Health II");
        itemMaker.setAmount(2);
        return itemMaker.getItemStack();
    }

    public static ItemStack getFood() {
        ItemMaker itemMaker = new ItemMaker(Material.SPLASH_POTION);
        itemMaker.addPotionEffect(PotionEffectType.REGENERATION.createEffect(3000, 3));
        itemMaker.setDisplayName("&2Regeneration Potion");
        itemMaker.addFlag(ItemFlag.HIDE_POTION_EFFECTS);
        itemMaker.setLore("&aRegeneration III (0:32)");
        itemMaker.setAmount(3);
        return itemMaker.getItemStack();
    }

    public static ItemStack getDamager() {
        ItemMaker itemMaker = new ItemMaker(Material.SPLASH_POTION);
        itemMaker.addPotionEffect(PotionEffectType.HARM.createEffect(20, 1));
        itemMaker.setDisplayName("&2Damage Potion");
        itemMaker.addFlag(ItemFlag.HIDE_POTION_EFFECTS);
        itemMaker.setLore("&cInstant Damage I");
        itemMaker.setAmount(4);
        return itemMaker.getItemStack();
    }

    public static ItemStack getBow() {
        return new ItemMaker(Material.AIR).getItemStack();
    }

    public static ItemStack getArrow() {
        return new ItemMaker(Material.AIR).getItemStack();
    }

    public static void toPlayer(Player player) {
        PlayerInventory i = player.getInventory();
        i.setItem(0, getWeapon());
        i.setItem(1, getFood());
        i.setItem(2, getDamager());
    }
}
