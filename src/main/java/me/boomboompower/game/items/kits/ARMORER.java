/*
 *     Copyright (C) 2016 boomboompower
 *
 *     This program is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     This program is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU General Public License for more details.
 *
 *     You should have received a copy of the GNU General Public License
 *     along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package me.boomboompower.game.items.kits;

import me.boomboompower.game.makers.ItemMaker;

import org.bukkit.Material;
import org.bukkit.enchantments.Enchantment;
import org.bukkit.entity.Player;
import org.bukkit.inventory.ItemFlag;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.PlayerInventory;

public class ARMORER {

    private ARMORER() {}

    public static ItemStack getIcon() {
        ItemMaker itemMaker = new ItemMaker(Material.GOLD_CHESTPLATE);
        itemMaker.setName("&aArmorer", true);
        itemMaker.setUnbreakable(true);
        itemMaker.setLore("&7Golden chestplate &8(&7Unbreaking V&8)", "&7Golden leggings &8(&7Unbreaking V&8)", "&7Golden boots &8(&7Unbreaking V&8)");
        itemMaker.addFlag(ItemFlag.HIDE_ATTRIBUTES);
        return itemMaker.getItemStack();
    }

    public static ItemStack getHelmet() {
        return new ItemMaker(Material.AIR).getItemStack();
    }

    public static ItemStack getChestPlate() {
        ItemMaker itemMaker = new ItemMaker(Material.GOLD_CHESTPLATE);
        itemMaker.setUnbreakable(false);
        itemMaker.setDisplayName("&2Armorer Chestplate");
        itemMaker.addEnchantment(Enchantment.DURABILITY, 5);
        itemMaker.setLore("&9Protect yourself.");
        return itemMaker.getItemStack();
    }

    public static ItemStack getLeggings() {
        ItemMaker itemMaker = new ItemMaker(Material.GOLD_LEGGINGS);
        itemMaker.setUnbreakable(false);
        itemMaker.setDisplayName("&2Armorer Leggings");
        itemMaker.addEnchantment(Enchantment.DURABILITY, 5);
        itemMaker.setLore("&9Protect yourself.");
        return itemMaker.getItemStack();
    }

    public static ItemStack getBoots() {
        ItemMaker itemMaker = new ItemMaker(Material.GOLD_BOOTS);
        itemMaker.setUnbreakable(false);
        itemMaker.setDisplayName("&2Armorer Boots");
        itemMaker.addEnchantment(Enchantment.DURABILITY, 5);
        itemMaker.setLore("&9Protect yourself.");
        return itemMaker.getItemStack();
    }

    public static ItemStack getWeapon() {
        return new ItemMaker(Material.AIR).getItemStack();
    }

    public static ItemStack getFood() {
        return new ItemMaker(Material.AIR).getItemStack();
    }

    public static ItemStack getBow() {
        return new ItemMaker(Material.AIR).getItemStack();
    }

    public static ItemStack getArrow() {
        return new ItemMaker(Material.AIR).getItemStack();
    }

    public static void toPlayer(Player player) {
        PlayerInventory i = player.getInventory();
        i.setItem(0, getWeapon());
        i.setItem(1, getFood());
        i.setItem(8, getBow());
        i.setItem(27, getArrow());
        i.setHelmet(getHelmet());
        i.setChestplate(getChestPlate());
        i.setLeggings(getLeggings());
        i.setBoots(getBoots());
    }
}
