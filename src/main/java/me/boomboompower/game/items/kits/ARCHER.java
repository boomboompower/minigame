/*
 *     Copyright (C) 2016 boomboompower
 *
 *     This program is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     This program is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU General Public License for more details.
 *
 *     You should have received a copy of the GNU General Public License
 *     along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package me.boomboompower.game.items.kits;

import me.boomboompower.game.makers.ItemMaker;

import org.bukkit.Material;
import org.bukkit.enchantments.Enchantment;
import org.bukkit.entity.Player;
import org.bukkit.inventory.ItemFlag;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.PlayerInventory;

public class ARCHER {

    private ARCHER() {}

    public static ItemStack getIcon() {
        ItemMaker itemMaker = new ItemMaker(Material.BOW);
        itemMaker.setName("&aArcher", true);
        itemMaker.setUnbreakable(true);
        itemMaker.setLore("&7Iron boots &8(&7Projectile Protection V&8)", "&7Stone axe &8(&7Sharpness I&8)", "&7Bow &8(&7Power II&8)", "&7Cooked Mutton &8x8", "&7Arrow &8x16");
        itemMaker.addFlag(ItemFlag.HIDE_ATTRIBUTES);
        return itemMaker.getItemStack();
    }

    public static ItemStack getHelmet() {
        return new ItemMaker(Material.AIR).getItemStack();
    }

    public static ItemStack getChestPlate() {
        return new ItemMaker(Material.AIR).getItemStack();
    }

    public static ItemStack getLeggings() {
        return new ItemMaker(Material.AIR).getItemStack();
    }

    public static ItemStack getBoots() {
        ItemMaker itemMaker = new ItemMaker(Material.IRON_BOOTS);
        itemMaker.setUnbreakable(false);
        itemMaker.setDisplayName("&2Archer Boots");
        itemMaker.addEnchantment(Enchantment.PROTECTION_PROJECTILE, 5);
        itemMaker.setLore("&9Shoot them down.");
        return itemMaker.getItemStack();
    }

    public static ItemStack getWeapon() {
        ItemMaker itemMaker = new ItemMaker(Material.STONE_AXE);
        itemMaker.setUnbreakable(false);
        itemMaker.setDisplayName("&2Archer Weapon");
        itemMaker.addEnchantment(Enchantment.DAMAGE_ALL, 1);
        itemMaker.setLore("&9Shoot them down.");
        return itemMaker.getItemStack();
    }

    public static ItemStack getFood() {
        ItemMaker itemMaker = new ItemMaker(Material.COOKED_MUTTON);
        itemMaker.setUnbreakable(true);
        itemMaker.setDisplayName("&2Archer Food");
        itemMaker.setLore("&9Shoot them down.");
        itemMaker.setAmount(8);
        return itemMaker.getItemStack();
    }

    public static ItemStack getBow() {
        ItemMaker itemMaker = new ItemMaker(Material.BOW);
        itemMaker.setUnbreakable(false);
        itemMaker.setDisplayName("&2Archer Bow");
        itemMaker.addEnchantment(Enchantment.ARROW_DAMAGE, 2);
        itemMaker.setLore("&9Shoot them down.");
        return itemMaker.getItemStack();
    }

    public static ItemStack getArrow() {
        ItemMaker itemMaker = new ItemMaker(Material.ARROW);
        itemMaker.setUnbreakable(false);
        itemMaker.setDisplayName("&2Archer Arrow");
        itemMaker.setLore("&9Shoot them down");
        itemMaker.setAmount(16);
        return itemMaker.getItemStack();
    }

    public static void toPlayer(Player player) {
        PlayerInventory i = player.getInventory();
        i.setItem(0, getWeapon());
        i.setItem(1, getFood());
        i.setItem(8, getBow());
        i.setItem(27, getArrow());
        i.setHelmet(getHelmet());
        i.setChestplate(getChestPlate());
        i.setLeggings(getLeggings());
        i.setBoots(getBoots());
    }
}
