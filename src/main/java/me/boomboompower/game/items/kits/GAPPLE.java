/*
 *     Copyright (C) 2016 boomboompower
 *
 *     This program is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     This program is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU General Public License for more details.
 *
 *     You should have received a copy of the GNU General Public License
 *     along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package me.boomboompower.game.items.kits;

import me.boomboompower.game.makers.ItemMaker;

import org.bukkit.Material;
import org.bukkit.enchantments.Enchantment;
import org.bukkit.entity.Player;
import org.bukkit.inventory.ItemFlag;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.PlayerInventory;

public class GAPPLE {

    private GAPPLE() {}

    public static ItemStack getIcon() {
        ItemMaker itemMaker = new ItemMaker(new ItemStack(Material.GOLDEN_APPLE, 1, (byte) 1));
        itemMaker.setName("&aGapple", true);
        itemMaker.setUnbreakable(true);
        itemMaker.setLore("&7Chainmail chestplate &8(&7Fire protection I&8)", "&7Wood axe &8(&7Dig speed V&8)", "&7Golden apple &8x2");
        itemMaker.addFlag(ItemFlag.HIDE_ATTRIBUTES);
        return itemMaker.getItemStack();
    }

    public static ItemStack getHelmet() {
        return new ItemMaker(Material.AIR).getItemStack();
    }

    public static ItemStack getChestPlate() {
        ItemMaker itemMaker = new ItemMaker(Material.CHAINMAIL_CHESTPLATE);
        itemMaker.setUnbreakable(false);
        itemMaker.setDisplayName("&2Gapple Chestplate");
        itemMaker.addEnchantment(Enchantment.PROTECTION_FIRE, 1);
        itemMaker.setLore("&9Overpower them.");
        return itemMaker.getItemStack();
    }

    public static ItemStack getLeggings() {
        return new ItemMaker(Material.AIR).getItemStack();
    }

    public static ItemStack getBoots() {
        return new ItemMaker(Material.AIR).getItemStack();
    }

    public static ItemStack getWeapon() {
        ItemMaker itemMaker = new ItemMaker(Material.WOOD_AXE);
        itemMaker.setUnbreakable(false);
        itemMaker.setDisplayName("&2Gapple Axe");
        itemMaker.addEnchantment(Enchantment.DIG_SPEED, 5);
        itemMaker.setLore("&9Overpower them.");
        return itemMaker.getItemStack();
    }

    public static ItemStack getFood() {
        ItemMaker itemMaker = new ItemMaker(Material.GOLDEN_APPLE);
        itemMaker.setUnbreakable(true);
        itemMaker.setDisplayName("&2Gapple Food");
        itemMaker.addEnchantment(Enchantment.DURABILITY, 10);
        itemMaker.setLore("&9Overpower them.");
        itemMaker.setAmount(2);
        return itemMaker.getItemStack();
    }

    public static ItemStack getBow() {
        return new ItemMaker(Material.AIR).getItemStack();
    }

    public static ItemStack getArrow() {
        return new ItemMaker(Material.AIR).getItemStack();
    }

    public static void toPlayer(Player player) {
        PlayerInventory i = player.getInventory();
        i.setItem(0, getWeapon());
        i.setItem(1, getFood());
        i.setItem(8, getBow());
        i.setItem(27, getArrow());
        i.setHelmet(getHelmet());
        i.setChestplate(getChestPlate());
        i.setLeggings(getLeggings());
        i.setBoots(getBoots());
    }
}
