/*
 *     Copyright (C) 2016 boomboompower
 *
 *     This program is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     This program is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU General Public License for more details.
 *
 *     You should have received a copy of the GNU General Public License
 *     along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package me.boomboompower.game.items.kits;

import me.boomboompower.game.makers.ItemMaker;

import org.bukkit.Material;
import org.bukkit.enchantments.Enchantment;
import org.bukkit.entity.Player;
import org.bukkit.inventory.ItemFlag;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.PlayerInventory;

public class KNIGHT {

    private KNIGHT() {}

    public static ItemStack getIcon() {
        ItemMaker itemMaker = new ItemMaker(Material.STONE_SWORD);
        itemMaker.setName("&aKnight", true);
        itemMaker.setUnbreakable(true);
        itemMaker.setLore("&7Leather helmet &8(&7Protection I&8)", "&7Leather chestplate &8(&7Unbreaking III&8)", "&7Iron sword", "&7Bread &8x6");
        itemMaker.addFlag(ItemFlag.HIDE_ATTRIBUTES);
        return itemMaker.getItemStack();
    }

    public static ItemStack getHelmet() {
        ItemMaker itemMaker = new ItemMaker(Material.LEATHER_HELMET);
        itemMaker.setUnbreakable(false);
        itemMaker.setDisplayName("&2Knight Helmet");
        itemMaker.addEnchantment(Enchantment.PROTECTION_ENVIRONMENTAL, 1);
        itemMaker.setLore("&9Kill them all.");
        return itemMaker.getItemStack();
    }

    public static ItemStack getChestPlate() {
        ItemMaker itemMaker = new ItemMaker(Material.LEATHER_CHESTPLATE);
        itemMaker.setUnbreakable(false);
        itemMaker.setDisplayName("&2Knight Chestplate");
        itemMaker.addEnchantment(Enchantment.DURABILITY, 3);
        itemMaker.setLore("&9Kill them all.");
        return itemMaker.getItemStack();
    }

    public static ItemStack getLeggings() {
        return new ItemMaker(Material.AIR).getItemStack();
    }

    public static ItemStack getBoots() {
        return new ItemMaker(Material.AIR).getItemStack();
    }

    public static ItemStack getWeapon() {
        ItemMaker itemMaker = new ItemMaker(Material.IRON_SWORD);
        itemMaker.setUnbreakable(false);
        itemMaker.setDisplayName("&2Knight Sword");
        itemMaker.setLore("&9Kill them all.");
        return itemMaker.getItemStack();
    }

    public static ItemStack getFood() {
        ItemMaker itemMaker = new ItemMaker(Material.BREAD);
        itemMaker.setUnbreakable(true);
        itemMaker.setDisplayName("&2Stale Bread");
        itemMaker.setLore("&9Kill them all");
        itemMaker.setAmount(12);
        return itemMaker.getItemStack();
    }

    public static ItemStack getBow() {
        return new ItemMaker(Material.AIR).getItemStack();
    }

    public static ItemStack getArrow() {
        return new ItemMaker(Material.AIR).getItemStack();
    }

    public static void toPlayer(Player player) {
        PlayerInventory i = player.getInventory();
        i.setItem(0, getWeapon());
        i.setItem(1, getFood());
        i.setItem(8, getBow());
        i.setItem(27, getArrow());
        i.setHelmet(getHelmet());
        i.setChestplate(getChestPlate());
        i.setLeggings(getLeggings());
        i.setBoots(getBoots());
    }
}
