/*
 *     Copyright (C) 2016 boomboompower
 *
 *     This program is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     This program is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU General Public License for more details.
 *
 *     You should have received a copy of the GNU General Public License
 *     along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package me.boomboompower.game.types;

import me.boomboompower.game.exceptions.UnknownStateException;

public enum Data {

    LOADING(0x0),
    WAITING(0x2),
    STARTED(0x4),
    RESETTING(0x6),
    BROKEN(0x8),
    UNKNOWN(0x10);

    private int code;

    Data(int code) {
        this.code = code;
    }

    public String getMessage() {
        switch (code) {
            case 0x0:
                return "The game is currently loading";
            case 0x2:
                return "The game is currently waiting";
            case 0x4:
                return "The game has started";
            case 0x6:
                return "The game is resetting";
            case 0x8:
                return "An error has occured";
            case 0x10:
                return "The game status is currently unknown";
            default:
                throw new UnknownStateException("Ensure the hexadecimal code is valid.");
        }
    }
}
