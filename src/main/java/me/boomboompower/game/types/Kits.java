/*
 *     Copyright (C) 2016 boomboompower
 *
 *     This program is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     This program is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU General Public License for more details.
 *
 *     You should have received a copy of the GNU General Public License
 *     along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package me.boomboompower.game.types;

public enum Kits {

    DEFAULT(Rarity.DEFAULT),
    ARCHER(Rarity.COMMON),
    ARMORER(Rarity.COMMON),
    FISHERMAN(Rarity.UNCOMMON),
    KNIGHT(Rarity.UNCOMMON),
    WITCH(Rarity.RARE),
    PYRO(Rarity.RARE),
    GAPPLE(Rarity.LEGENDARY);

    private Rarity rarity;

    Kits(Rarity rarity) {
        this.rarity = rarity;
    }

    public Rarity getRarity() {
        return rarity != null ? rarity : Rarity.DEFAULT;
    }
}
