/*
 *     Copyright (C) 2016 boomboompower
 *
 *     This program is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     This program is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU General Public License for more details.
 *
 *     You should have received a copy of the GNU General Public License
 *     along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package me.boomboompower.game.listeners;

import me.boomboompower.game.Game;
import me.boomboompower.game.types.GameState;
import me.boomboompower.game.api.MessageUtils;

import org.bukkit.Bukkit;
import org.bukkit.Location;
import org.bukkit.Material;
import org.bukkit.block.Block;
import org.bukkit.block.BlockState;
import org.bukkit.block.Chest;
import org.bukkit.event.EventHandler;
import org.bukkit.event.EventPriority;
import org.bukkit.event.Listener;
import org.bukkit.event.block.Action;
import org.bukkit.event.block.BlockBreakEvent;
import org.bukkit.event.block.BlockPlaceEvent;
import org.bukkit.event.player.PlayerInteractEvent;

import java.util.ArrayList;

public class WorldListeners implements Listener {

    private Game game;
    private ArrayList<Location> blocks = new ArrayList<Location>();

    public WorldListeners(Game game) {
        this.game = game;

        Bukkit.getPluginManager().registerEvents(this, game);
    }

    @EventHandler(priority = EventPriority.HIGHEST)
    private void onBlockBreak(BlockBreakEvent event) {
        Block block = event.getBlock();
        if (game.getGameState() != GameState.STARTED) event.setCancelled(true);
        if (block.getType() == Material.CHEST || block.getType() == Material.TRAPPED_CHEST || block.getType() == Material.ENDER_CHEST) {
            MessageUtils.sendMessage(event.getPlayer(), "&7[&4&lINFO&] &cCannot break chests!");
            event.setCancelled(true);
        }
    }

    @EventHandler
    private void onBlockPlace(BlockPlaceEvent event) {
        if (game.getGameState() != GameState.STARTED) event.setCancelled(true);
    }

    @EventHandler(priority = EventPriority.HIGHEST)
    private void onChestOpen(PlayerInteractEvent event) {
        Block block = event.getClickedBlock();
        if (event.getAction().equals(Action.RIGHT_CLICK_BLOCK)) {
            Location loc = block.getLocation();
            if (block.getType() == Material.CHEST || block.getType() == Material.TRAPPED_CHEST) {
                if (!blocks.contains(loc)){
                    BlockState state = event.getClickedBlock().getState();
                    if (block instanceof Chest) {
                        Chest chest = (Chest) block;
                        blocks.add(event.getClickedBlock().getLocation());
                        Bukkit.dispatchCommand(Bukkit.getConsoleSender(), "/blockdata " + loc.getBlockX() + " " + loc.getBlockY() + " " + loc.getBlockZ() + " {LootTable:\"minecraft:custom/loot_table_boomboompower\"}");
                    }
                }
            }
        }
    }
}
