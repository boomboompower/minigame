/*
 *     Copyright (C) 2016 boomboompower
 *
 *     This program is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     This program is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU General Public License for more details.
 *
 *     You should have received a copy of the GNU General Public License
 *     along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package me.boomboompower.game.listeners;

import me.boomboompower.game.Game;
import me.boomboompower.game.api.MessageUtils;
import me.boomboompower.game.events.GameStateChangeEvent;
import me.boomboompower.game.items.KitSelector;
import me.boomboompower.game.makers.ConfigGetter;
import me.boomboompower.game.types.GameState;
import org.bukkit.Bukkit;
import org.bukkit.Effect;
import org.bukkit.GameMode;
import org.bukkit.Location;
import org.bukkit.attribute.Attribute;
import org.bukkit.entity.Player;
import org.bukkit.entity.Projectile;
import org.bukkit.event.EventHandler;
import org.bukkit.event.EventPriority;
import org.bukkit.event.Listener;
import org.bukkit.event.entity.*;
import org.bukkit.event.inventory.PrepareAnvilEvent;
import org.bukkit.event.player.*;
import org.spigotmc.SpigotConfig;

import java.util.ArrayList;
import java.util.Random;

public class PlayerListeners implements Listener {

    private Game game;
    private ArrayList<Player> online = new ArrayList<Player>();
    public ArrayList<Projectile> arrow = new ArrayList<Projectile>();

    public PlayerListeners(Game game) {
        this.game = game;

        Bukkit.getPluginManager().registerEvents(this, game);

        Bukkit.getScheduler().scheduleSyncRepeatingTask(game, new Runnable() {
            @Override
            public void run() {
                for(Projectile arrows : arrow) {
                    for(Player online : Bukkit.getOnlinePlayers()){
                        Location loc = arrows.getLocation();
                        online.getWorld().playEffect(loc, Effect.COLOURED_DUST, 5);
                    }
                }
            }

        }, 0, 1);
    }

    @EventHandler(priority = EventPriority.HIGHEST)
    private void onAnvilPrepare(PrepareAnvilEvent event) {
        if (event.getInventory().contains(KitSelector.getSelector())) {
            for (int i = 0; i < event.getViewers().size(); i++)
                event.getViewers().get(i).closeInventory();
        }
    }

    @EventHandler(priority = EventPriority.HIGHEST)
    private void onItemDrop(PlayerDropItemEvent event) {
        if (game.getGameState() == GameState.STARTED) {
            if (event.getItemDrop().getItemStack().equals(KitSelector.getSelector())) {
                event.setCancelled(true);
            }
        } else {
            event.setCancelled(true);
        }
    }

    @EventHandler(priority = EventPriority.HIGHEST)
    private void onQuit(PlayerQuitEvent event) {
        final Player p = event.getPlayer();
        if (online.contains(p)) {
            event.setQuitMessage(MessageUtils.translate("&8[&c-&8] &b" + p.getName() + " has left."));
        } else {
            event.setQuitMessage("");
        }
    }

    @EventHandler(priority = EventPriority.HIGHEST)
    private void onJoin(PlayerJoinEvent event) {
        final Player p = event.getPlayer();
        final int players = Bukkit.getOnlinePlayers().size();
        switch (game.getGameState()) {
            case WAITING:
                if (players > game.getMaxPlayers()) {
                    p.kickPlayer(MessageUtils.translate("&cGame is full, sorry!"));
                }
                game.setPlayers(players);
                online.add(p);
                event.setJoinMessage(MessageUtils.translate("&8[&a+&8] &b" + p.getName() + " has joined. &3(" + game.getPlayerAmount() + "/" + game.getMaxPlayers() + ")" ));

                // BEGIN - Player Options
                p.setGameMode(GameMode.SURVIVAL);
                p.getAttribute(Attribute.GENERIC_ATTACK_SPEED).setBaseValue(16.0D);
                p.getInventory().clear();
                p.getActivePotionEffects().clear();
                p.getInventory().setItem(4, KitSelector.getSelector());
                // END - Player Options

                if (players >= game.getMinPlayers()) {
                    game.setGameState(GameState.STARTING);
                }
                break;
            default:
                event.setJoinMessage("");
                if (!p.isOp()) {
                    p.kickPlayer(MessageUtils.translate("&cGame has started, sorry!"));
                }
                break;
        }
    }

    @EventHandler
    private void onGameStateChange(GameStateChangeEvent event) {
        if (event.getNewGameState() == GameState.STARTED) {
            for (Player player : online) {
                teleportToArena(player);
            }
        }
    }

    @EventHandler
    private void onCommandProcess(PlayerCommandPreprocessEvent event) {
        if (!event.getPlayer().hasPermission(Game.PERMISSION + "command")) {
            MessageUtils.sendActionbar(event.getPlayer(), SpigotConfig.unknownCommandMessage);
            event.setCancelled(true);
        }
    }

    @EventHandler
    private void onBowFire(EntityShootBowEvent event) {
        if (event.getEntity() instanceof Player) {
            Player p = (Player) event.getEntity();
            event.getProjectile().setCustomName(p.getName());
            arrow.add((Projectile) event.getProjectile());
        }
    }

    @EventHandler(priority = EventPriority.HIGHEST)
    private void onArrowLand(ProjectileHitEvent event) {
        if (arrow.contains(event.getEntity())) {
            arrow.remove(event.getEntity());
        }
    }

    @EventHandler(priority = EventPriority.HIGHEST)
    private void onPVP(EntityDamageByEntityEvent event) {
        if (game.getGameState() == GameState.STARTED) {
            if (event.getEntity() instanceof Player && event.getDamager() instanceof Player) {
                Player p = (Player) event.getEntity();
                Player d = (Player) event.getDamager();
            }
        }
    }

    @EventHandler(priority = EventPriority.HIGHEST)
    private void onDeath(PlayerDeathEvent event) {
        if (game.getGameState() == GameState.WAITING || game.getGameState() == GameState.STARTING) {
            event.setKeepInventory(true);
        } else {
            if (event.getEntity().getKiller() != null) {
                Player k = event.getEntity().getKiller();
                ConfigGetter c = new ConfigGetter(k);

                addRandomGold(k);
                if (c.getHealth() >= 1) {
                    k.setHealth(k.getHealth() + (c.getHealth() * 2));
                    MessageUtils.sendMessage(k, "&aReceived " + (c.getHealth() * 2) + "❤");
                }
            }
        }
    }

    @EventHandler(priority = EventPriority.HIGHEST)
    private void onDamage(EntityDamageEvent event) {
        if (event.getCause() == EntityDamageEvent.DamageCause.FALL) event.setCancelled(true);
    }

    private void teleportToArena(Player p) {
        p.teleport(new Location(p.getWorld(), 0, 50, 0), PlayerTeleportEvent.TeleportCause.SPECTATE);
    }

    private void addRandomGold(Player p) {
        ConfigGetter c = new ConfigGetter(p);
        String s = "\uD83D\uDCB0";
        int g = 3;

        if (new Random().nextInt(10) > 9) {
            g = 7;
            s = "\uD83D\uDCB5";
        }
        MessageUtils.sendActionbar(p, "&a&lReceived &2&l" + g + "&a&l gold! &5" + s);
    }
}
