/*
 *     Copyright (C) 2016 boomboompower
 *
 *     This program is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     This program is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU General Public License for more details.
 *
 *     You should have received a copy of the GNU General Public License
 *     along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package me.boomboompower.game.listeners;

import me.boomboompower.game.Game;
import me.boomboompower.game.inventories.ConfirmationGUI;
import me.boomboompower.game.inventories.SelectorGUI;
import me.boomboompower.game.items.KitSelector;
import me.boomboompower.game.items.kits.*;

import org.bukkit.Bukkit;
import org.bukkit.Material;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.EventPriority;
import org.bukkit.event.Listener;
import org.bukkit.event.inventory.InventoryClickEvent;
import org.bukkit.event.player.PlayerInteractEvent;
import org.bukkit.inventory.EquipmentSlot;
import org.bukkit.inventory.Inventory;
import org.bukkit.inventory.ItemStack;

import java.util.List;

public class KitListeners implements Listener {

    private Game game;

    public KitListeners(Game game) {
        this.game = game;

        Bukkit.getPluginManager().registerEvents(this, game);
    }

    @EventHandler(priority = EventPriority.HIGHEST)
    private void onPlayerInteract(PlayerInteractEvent event) {
        Player player = event.getPlayer();
        ItemStack item = player.getInventory().getItemInMainHand();

        if (event.getHand() != EquipmentSlot.HAND) return;

        if (item.equals(KitSelector.getSelector())) {
            player.openInventory(SelectorGUI.getGUI());
        }
    }

    @EventHandler(priority = EventPriority.HIGHEST)
    private void onInventoryInteract(InventoryClickEvent event) {
        Inventory inventory = event.getInventory();
        Player p = (Player) event.getWhoClicked();
        ItemStack item = event.getCurrentItem();
        if (inventory.getTitle().equals(SelectorGUI.getGUI().getTitle())) {
            if (item.getType() == Material.AIR) return;
            if (item.getType() == Material.STONE_PICKAXE) {
                a(p, "Default");
            } else if (item.equals(GAPPLE.getIcon())) {
                a(p, "Gapple");
            } else if (item.equals(KNIGHT.getIcon())) {
                a(p, "Knight");
            } else if (item.equals(ARMORER.getIcon())) {
                a(p, "Armorer");
            } else if (item.equals(ARCHER.getIcon())) {
                a(p, "Archer");
            } else if (item.equals(FISHERMAN.getIcon())) {
                a(p, "Fisherman");
            } else if (item.equals(PYRO.getIcon())) {
                a(p, "Pyro");
            } else if (item.equals(WITCH.getIcon())) {
                a(p, "Witch");
            }
        } else if (inventory.getTitle().equals(new ConfirmationGUI().getGUI().getTitle())) {
            List<String> lore = item.getItemMeta().getLore();
            if (lore == null || item.getType() == Material.AIR) return;
            if (lore.toString().contains("Default")) {
                a(p);
                DEFAULT.toPlayer(p);
            } else if (lore.toString().contains("Gapple")) {
                a(p);
                GAPPLE.toPlayer(p);
            } else if (lore.toString().contains("Knight")) {
                a(p);
                KNIGHT.toPlayer(p);
            } else if (lore.toString().contains("Armorer")) {
                a(p);
                ARMORER.toPlayer(p);
            } else if (lore.toString().contains("Archer")) {
                a(p);
                ARCHER.toPlayer(p);
            } else if (lore.toString().contains("Fisherman")) {
                a(p);
                FISHERMAN.toPlayer(p);
            } else if (lore.toString().contains("Pyro")) {
                a(p);
                PYRO.toPlayer(p);
            } else if (lore.toString().contains("Witch")) {
                a(p);
                WITCH.toPlayer(p);
            } else {
                a(p, SelectorGUI.getGUI());
            }
        }
    }

    private void a(Player p) {
        p.closeInventory();
        p.getInventory().clear();
    }

    private void a(Player p, Inventory i) {
        p.openInventory(i);
    }

    private void a(Player p, String s) {
        a(p, new ConfirmationGUI(s).getGUI());
    }
}
