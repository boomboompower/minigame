/*
 *     Copyright (C) 2016 boomboompower
 *
 *     This program is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     This program is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU General Public License for more details.
 *
 *     You should have received a copy of the GNU General Public License
 *     along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package me.boomboompower.game.inventories;

import me.boomboompower.game.makers.InventoryMaker;
import me.boomboompower.game.makers.ItemMaker;
import org.bukkit.Material;
import org.bukkit.inventory.Inventory;
import org.bukkit.inventory.ItemStack;

import java.util.Random;

public class SoulRollingGUI {

    private SoulRollingGUI() {}

/*          Inventory Slot Numbers
    0  | 1  | 2  | 3  | 4  | 5  | 6  | 7  | 8
    9  | 10 | 11 | 12 | 13 | 14 | 15 | 16 | 17
    18 | 19 | 20 | 21 | 22 | 23 | 24 | 25 | 26
    27 | 28 | 29 | 30 | 31 | 32 | 33 | 34 | 35
    36 | 37 | 28 | 29 | 40 | 41 | 42 | 43 | 44
    45 | 46 | 47 | 48 | 49 | 50 | 51 | 52 | 53
    54 | 55 | 56 | 57 | 58 | 59 | 60 | 61 | 62
*/

    public static Inventory getGUI() {
        InventoryMaker inventoryMaker = new InventoryMaker("&4Soul Well", 63);
        animate(inventoryMaker);
        return inventoryMaker.getInventory();
    }

    private static void animate(InventoryMaker inventory) {
        for (int i = 0; i <= 26; i++) {
            int r = new Random().nextInt(10);
            if (r > 8) inventory.setItem(pane(15), i);
            else inventory.setItem(pane(14), i);
        } for (int i = 36; i <= 62; i++) {
            int r = new Random().nextInt(10);
            if (r > 8) inventory.setItem(pane(15), i);
            else inventory.setItem(pane(14), i);
        }
    }

    private static ItemStack pane(int code) {
        ItemMaker itemMaker = new ItemMaker(new ItemStack(Material.STAINED_GLASS_PANE, 1, (short) code));
        itemMaker.setUnbreakable(true);
        itemMaker.setName("&8Rolling...", true);
        return itemMaker.getItemStack();
    }
}
