/*
 *     Copyright (C) 2016 boomboompower
 *
 *     This program is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     This program is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU General Public License for more details.
 *
 *     You should have received a copy of the GNU General Public License
 *     along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package me.boomboompower.game.inventories;

import me.boomboompower.game.items.kits.*;

import me.boomboompower.game.makers.InventoryMaker;
import org.bukkit.inventory.Inventory;

public class SelectorGUI {

    private SelectorGUI() {}

/*          Inventory Slot Numbers
    0  | 1  | 2  | 3  | 4  | 5  | 6  | 7  | 8
    9  | 10 | 11 | 12 | 13 | 14 | 15 | 16 | 17
    18 | 19 | 20 | 21 | 22 | 23 | 24 | 25 | 26
    27 | 28 | 29 | 30 | 31 | 32 | 33 | 34 | 35
    36 | 37 | 28 | 29 | 40 | 41 | 42 | 43 | 44
    45 | 46 | 47 | 48 | 49 | 50 | 51 | 52 | 53
*/

    public static Inventory getGUI() {
        InventoryMaker inventoryMaker = new InventoryMaker("&4&lSelect a kit!", 54);
        inventoryMaker.setItem(DEFAULT.getIcon(), 20);
        inventoryMaker.setItem(KNIGHT.getIcon(), 21);
        inventoryMaker.setItem(ARMORER.getIcon(), 22);
        inventoryMaker.setItem(ARCHER.getIcon(), 23);
        inventoryMaker.setItem(FISHERMAN.getIcon(), 24);
        inventoryMaker.setItem(PYRO.getIcon(), 29);
        inventoryMaker.setItem(WITCH.getIcon(), 30);
        inventoryMaker.setItem(GAPPLE.getIcon(), 31);
//       inventoryMaker.setItem(, 32);

        return inventoryMaker.getInventory();
    }
}
