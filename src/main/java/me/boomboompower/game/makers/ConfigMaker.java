/*
 *     Copyright (C) 2016 boomboompower
 *
 *     This program is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     This program is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU General Public License for more details.
 *
 *     You should have received a copy of the GNU General Public License
 *     along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package me.boomboompower.game.makers;

import me.boomboompower.game.Game;

import org.bukkit.Material;
import org.bukkit.configuration.file.FileConfiguration;
import org.bukkit.entity.Player;
import org.bukkit.inventory.ItemStack;

public class ConfigMaker {

    private String uuid;

    public ConfigMaker(Player player) {
        this.uuid = player.getUniqueId().toString();
    }

    public String getString(String key) {
        return getConfig().getString(parse(key)) != null ? getConfig().getString(parse(key)) : "";
    }

    public int getInt(String key) {
        return getConfig().getInt(parse(key));
    }

    public ItemStack getItemStack(String key) {
        return getConfig().getItemStack(parse(key)) != null ? getConfig().getItemStack(parse(key)) : new ItemMaker(Material.AIR).getItemStack();
    }

    public String getKit() {
        return getString(parse("kit")) != null ? getString(parse("kit")) : "DEFAULT";
    }

    public void set(String path, Object setTo) {
        getConfig().set(parse(path), setTo);
    }

    private String parse(String key) {
        return uuid + "." + key;
    }

    public void savePlayer() {
        getConfig().createSection(uuid);
        set(parse("kit"), "DEFAULT");
        set(parse("health"), 0);
        set(parse("deaths"), 0);
        set(parse("kills"), 0);
        set(parse("wins"), 0);
        set(parse("loses"), 0);
    }

    public static void saveDefaultConfig() {
        Game.getInstance().saveDefaultConfig();
    }

    private FileConfiguration getConfig() {
        return Game.getInstance().getConfig();
    }
}
