/*
 *     Copyright (C) 2016 boomboompower
 *
 *     This program is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     This program is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU General Public License for more details.
 *
 *     You should have received a copy of the GNU General Public License
 *     along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package me.boomboompower.game.makers;

import org.bukkit.entity.Player;

public class ConfigGetter extends ConfigMaker {

    private String uuid;

    public ConfigGetter(Player player) {
        super(player);
        this.uuid = player.getUniqueId().toString();
    }

    public int getHealth() {
        return super.getInt("health");
    }

    public void setHealth(int health) {
        super.set("health", health);
    }

    public void addKill() {
        super.set("kills", super.getInt("kills") + 1);
    }
}
