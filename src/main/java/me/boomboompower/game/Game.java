/*
 *     Copyright (C) 2016 boomboompower
 *
 *     This program is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     This program is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU General Public License for more details.
 *
 *     You should have received a copy of the GNU General Public License
 *     along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package me.boomboompower.game;

import me.boomboompower.game.events.GameStateChangeEvent;
import me.boomboompower.game.listeners.RegisterListeners;

import me.boomboompower.game.types.GameState;
import org.bukkit.plugin.java.JavaPlugin;

public class Game extends JavaPlugin {

    private static Game game;
    private GameState gameState;
    private int minPlayers;
    private int maxPlayers;
    private int currentPlayers;

    public static final String PERMISSION = "game.";

    @Override
    public void onEnable() {
        game = this;
        this.minPlayers = 10;
        this.maxPlayers = 16;
        this.currentPlayers = 0;
        this.gameState = GameState.WAITING;

        new RegisterListeners(this);
    }

    public int getMaxPlayers() {
        return this.maxPlayers;
    }

    public int getMinPlayers() {
        return this.minPlayers;
    }

    public int getPlayerAmount() {
        return this.currentPlayers;
    }

    public void setPlayers(int amount) {
        this.currentPlayers = amount;
    }

    public void setGameState(GameState newState) {
        GameStateChangeEvent event = new GameStateChangeEvent(this.gameState, newState);
        if (!event.isCancelled()) gameState = event.getNewGameState();
    }

    public GameState getGameState() {
        return this.gameState;
    }

    public static Game getInstance() {
        return game;
    }
}
