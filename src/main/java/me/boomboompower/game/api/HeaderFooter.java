/*
 *     Copyright (C) 2016 boomboompower
 *
 *     This program is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     This program is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU General Public License for more details.
 *
 *     You should have received a copy of the GNU General Public License
 *     along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package me.boomboompower.game.api;

import org.bukkit.entity.Player;

public class HeaderFooter {

    public static void sendHeaderFooter(Player player, String header, String footer) {
        try {
            header = FormatUtils.translate(header);
            footer = FormatUtils.translate(footer);

            Object d = ReflectionUtils.getNMSClass("IChatBaseComponent").getDeclaredClasses()[0].getMethod("a", String.class).invoke(null, "{\"text\":\"" + header + "\"}");
            Object e = ReflectionUtils.getNMSClass("IChatBaseComponent").getDeclaredClasses()[0].getMethod("a", String.class).invoke(null, "{\"text\":\"" + footer + "\"}");
            Object f = ReflectionUtils.getNMSClass("PacketPlayOutPlayerListHeaderFooter").getConstructor(ReflectionUtils.getNMSClass("IChatBaseComponent")).newInstance(d);

            ReflectionUtils.setValue("a", f, d);
            ReflectionUtils.setValue("b", f, e);

            Object g = player.getClass().getMethod("getHandle").invoke(player);
            Object h = g.getClass().getField("playerConnection").get(g);

            h.getClass().getMethod("sendPacket", ReflectionUtils.getNMSClass("Packet")).invoke(h, f);
        } catch (Exception ex) {
            throw new RuntimeException("Failed to send a packet to " + player.getName() + ".");
        }
    }
}
