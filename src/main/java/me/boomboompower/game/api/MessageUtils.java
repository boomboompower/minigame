/*
 *     Copyright (C) 2016 boomboompower
 *
 *     This program is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     This program is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU General Public License for more details.
 *
 *     You should have received a copy of the GNU General Public License
 *     along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package me.boomboompower.game.api;

import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.block.CommandBlock;
import org.bukkit.command.CommandSender;
import org.bukkit.command.ConsoleCommandSender;
import org.bukkit.entity.Player;

public class MessageUtils {

    public static String translate(String text) {
        return ChatColor.translateAlternateColorCodes('&', text);
    }

    public static void sendActionbar(Player p, String message) {
        Actionbar.sendActionbar(p, translate(message));
    }

    public static void sendMessage(Player p, String message) {
        p.sendMessage(translate(message));
    }

    public static void log(String message) {
        Bukkit.getConsoleSender().sendMessage(translate(message));
    }

    public static void duelSend(CommandSender s, String message) {
        if (s instanceof Player) {
            sendMessage((Player) s, message);
        } else if (s instanceof ConsoleCommandSender) {
            log(message);
        } else if (s instanceof CommandBlock) {
            ((CommandBlock) s).setName(translate("&4But nobody came..."));
            ((CommandBlock) s).setCommand(translate("&4But nobody came..."));
            ((CommandBlock) s).update();
        } else {
            throw new IllegalStateException("That commandsender type is NOT supported!");
        }
    }

}
