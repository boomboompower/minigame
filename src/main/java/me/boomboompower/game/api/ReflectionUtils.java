/*
 *     Copyright (C) 2016 boomboompower
 *
 *     This program is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     This program is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU General Public License for more details.
 *
 *     You should have received a copy of the GNU General Public License
 *     along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package me.boomboompower.game.api;

import me.boomboompower.game.items.kits.*;
import me.boomboompower.game.types.Kits;
import org.bukkit.Bukkit;

import java.lang.reflect.Field;

public class ReflectionUtils {

    public static Field getValue(String fieldName, Object instance) {
        try {
            Field field = instance.getClass().getDeclaredField(fieldName);
            field.setAccessible(true);
            return field;
        } catch (Exception ex) {
            return null;
        }
    }

    public static void setValue(String fieldName, Object instance, Object value) {
        try {
            Field field = instance.getClass().getDeclaredField(fieldName);
            field.setAccessible(true);
            field.set(instance, value);
        } catch (Exception ex) {}
    }

    public static String getVersion() {
        return Bukkit.getServer().getClass().getPackage().getName().split("\\.")[3];
    }

    public static Class<?> getNMSClass(String name) {
        try {
            return Class.forName("net.minecraft.server." + getVersion() + "." + name);
        } catch (Exception ex) {
            return ReflectionUtils.class;
        }
    }

    public static Class<?> getKitClass(Kits kit) {
        Class<?> c = DEFAULT.class;
        switch (kit) {
            case DEFAULT:
                c = DEFAULT.class;
                break;
            case ARCHER:
                c = ARCHER.class;
                break;
            case ARMORER:
                c = ARMORER.class;
                break;
            case FISHERMAN:
                c = FISHERMAN.class;
                break;
            case KNIGHT:
                c = KNIGHT.class;
                break;
            case WITCH:
                c = WITCH.class;
                break;
            case PYRO:
                c = PYRO.class;
                break;
            case GAPPLE:
                c = GAPPLE.class;
                break;
        }
        return c;
    }

    public static Class<?> getCraftClass(String name) {
        try {
            return Class.forName("org.bukkit.craftbukkit." + getVersion() + "." + name);
        } catch (Exception e) {
            return ReflectionUtils.class;
        }
    }
}
