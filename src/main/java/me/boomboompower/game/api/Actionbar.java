/*
 *     Copyright (C) 2016 boomboompower
 *
 *     This program is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     This program is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU General Public License for more details.
 *
 *     You should have received a copy of the GNU General Public License
 *     along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package me.boomboompower.game.api;

import org.bukkit.entity.Player;

public class Actionbar {

    public static void sendActionbar(Player player, String message) {
        try {
            message = FormatUtils.translate(message);

            Object d = ReflectionUtils.getNMSClass("IChatBaseComponent").getDeclaredClasses()[0].getMethod("a", String.class).invoke(null, "{\"text\":\"" + message + "\"}");
            Object e = ReflectionUtils.getNMSClass("PacketPlayOutChat").getConstructor(ReflectionUtils.getNMSClass("IChatBaseComponent"), byte.class).newInstance(d, (byte) 2);

            Object f = player.getClass().getMethod("getHandle").invoke(player);
            Object g = f.getClass().getField("playerConnection").get(f);

            g.getClass().getMethod("sendPacket", ReflectionUtils.getNMSClass("Packet")).invoke(g, e);
        } catch (Exception e) {
            throw new RuntimeException("Error whilst sending actionbar");
        }
    }
}
